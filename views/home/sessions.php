<main>
    <section class="sessions" style='overflow: auto;'>

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo_blue.png"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

        <body>
            

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <div class='main_nav three_nav'>
                            <p id='keynote_nav' data-id='key' class='active'>KEYNOTES</p>
                            <p id='thought_nav' data-id='thought' class='height'>THOUGHT LEADERSHIP</p>
                            <p id='innovation_nav' data-id='innovations' class=''>INNOVATIONS</p>
                        </div>
                    </div>
                </div>

                <section id='key' class='active list'>
                    <div class="schedule_sessions timeit open speakers">
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/keynote1.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Walter Isaacson</p>
                                <p class='sub'>MONDAY, MAY 20 | 5:00 PM</p>
                                <p class="small green">American Writer and Journalist, Professor of History at Tulane</p>
                                <p class='par'>Journalist and historian Isaacson is best known in literary circles as the writer of magisterial biographies, scholarly and meticulously researched, yet immensely entertaining. </p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/keynote2.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Deborah Lee James</p>
                                <p class='sub'>WEDNESDAY, MAY 22 | 7:15 AM</p>
                                <p class="small green">Former Secretary of the Air Force, Senior Industry Executive, Board Member</p>
                                <p class='par'>James led strategic changes as well as streamlining operations and government-wide changes in space and cyber. James was president of SAIC’s technical and engineering sector.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/keynote3.png')"></div>
                            <div class='info'>
                                <p class="thin">Ian Morrison</p>
                                <p class='sub'>THURSDAY, MAY 23 | 9:00 AM</p>
                                <p class="small green">Author and Futurist specializing in the Future of Healthcare</p>
                                <p class='par'>Specializing in long-term forecasting in healthcare. Working with more than 100 Fortune 500 companies in healthcare, manufacturing, information technology and financial services.</p>
                            </div>
                            <div class='toggle_info'><p class='sign'>+</p></div>
                        </div>
                    </div>
                </section>

                <section id='thought' class='list'>
                    <div class="schedule_sessions timeit open speakers">
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought1.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Peter Borden</p>
                                <p class="sub">Chief Digital Officer, Cognizant</p>
                                <p class='small green'>Enterprise AI in Healthcare (Automate, Augment, Engage, Analysis)</p>
                                <p class='par'>This session will address the need for and enterprise-wide approach to artificial intelligence, and four things AI will do in healthcare today: automation, augmentation, engagement, analysis…and the need for a different technology. Join us for an operational approach for each area.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought2.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Gary Meyer</p>
                                <p class="sub">VP, Consulting, Cognizant</p>
                                <p class='small green'>Healthcare’s Five Must-Do Things in Blockchain in 2019</p>
                                <p class='par'>Blockchain is being taken up in the industry. Use cases are sorting out. Alliances are forming. Innovators are doing POCs and pilots and initial production solutions while others are standing by. What’s my organization to do? How will blockchain impact us? What can and should we do now? Hear from Cognizant’s Healthcare Blockchain Center of Excellence on the five must-do things for the industry. Learn about alliances, pilots, production solutions and how it relates to Cognizant product and service offerings. Benefit from practical next steps that you can bring to your organization, regardless of where you are on the journey.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought3.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Trish Birch</p>
                                <p class="sub">SVP and Global Practice Leader, Healthcare Consulting, Cognizant</p>
                                <p class='small green'>Healthcare “On-Demand”: Re-thinking Care Delivery and Health System Business Models for the Emerging Digital Economy</p>
                                <p class='par'>New Care Delivery business models are rapidly emerging across our industry: Internet care and “Virtual First” approaches, combined with retail-based health hubs are disrupting traditional Health System value propositions. These new Provider business and operating models have strategic implications for all players in the industry. During this session, Cognizant Consulting leaders will present our point of view on the emerging "on-demand healthcare" economy and the disruptive platforms that will enable it. We will highlight several steps stakeholders can take to manage the disruption while making “no-regrets” investments that will position them for success in the future.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought4.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Bill Shea</p>
                                <p class="sub">VP, Healthcare Business Consulting, Cognizant</p>
                                <p class='small green'>Healthcare “On-Demand”: Re-thinking Care Delivery and Health System Business Models for the Emerging Digital Economy</p>
                                <p class='par'>New Care Delivery business models are rapidly emerging across our industry: Internet care and “Virtual First” approaches, combined with retail-based health hubs are disrupting traditional Health System value propositions. These new Provider business and operating models have strategic implications for all players in the industry. During this session, Cognizant Consulting leaders will present our point of view on the emerging "on-demand healthcare" economy and the disruptive platforms that will enable it. We will highlight several steps stakeholders can take to manage the disruption while making “no-regrets” investments that will position them for success in the future.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought5.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Vanessa Pawlak</p>
                                <p class="sub">AVP, Consulting, Cognizant</p>
                                <p class='small green'>RegTech: Risk & Compliance in a Digital World</p>
                                <p class='par'>The post-reform era combined with digital evolution are causing an increase in non-compliance, data breaches, and enforcement agency pressures. The industry is faced with unprecedented challenges in maintaining operational regulatory adherence with federal and state requirements – all while staying cost effective. This session will explore digital means of achieving continuous compliance and what the future holds for the advancement and mainstreaming of regulatory technologies in a cost conscious, consumer-centric, enforcement intensified modern day healthcare world.</p>
                                <p class='small green' style='margin-top: 20px;'>The Future of Interoperability: Requirements for a New HIT Era</p>
                                <p class='par'>The post-reform era combined with digital evolution are causing an increase in non-compliance, data breaches, and enforcement agency pressures. The industry is faced with unprecedented challenges in maintaining operational regulatory adherence with federal and state requirements – all while staying cost effective. This session will explore digital means of achieving continuous compliance and what the future holds for the advancement and mainstreaming of regulatory technologies in a cost conscious, consumer-centric, enforcement intensified modern day healthcare world.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought6.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Patrick Spoletini</p>
                                <p class="sub">Global Practice Lead, Healthcare Provider Consulting, Cognizant</p>
                                <p class='small green'>Simplifying RCM in the Digital World</p>
                                <p class='par'>New technologies such as Artificial Intelligence (AI), Machine Learning, Blockchain and Automation are transforming the world of healthcare -- and Revenue Cycle Management. Attend this session to understand how to leverage these technologies to reduce costs, improve efficiency, increase revenues and deliver an enhanced customer experience. This session will share use case study examples that demonstrate how to apply AI, automation and Blockchain to specific RCM business operations; and best practices for introducing and managing human-driven, machine-augmented collaboration.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought7.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Srivaths Srinivasan</p>
                                <p class="sub">Director, Cognizant Consulting, Cognizant</p>
                                <p class='small green'>Simplifying RCM in the Digital World</p>
                                <p class='par'>New technologies such as Artificial Intelligence (AI), Machine Learning, Blockchain and Automation are transforming the world of healthcare -- and Revenue Cycle Management. Attend this session to understand how to leverage these technologies to reduce costs, improve efficiency, increase revenues and deliver an enhanced customer experience. This session will share use case study examples that demonstrate how to apply AI, automation and Blockchain to specific RCM business operations; and best practices for introducing and managing human-driven, machine-augmented collaboration.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought8.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Ryan Doyle</p>
                                <p class="sub">VP, Strategy, Idea Couture (A Cognizant Digital Business)</p>
                                <p class='small green'>The Future of Care: Enhancing Community Health & Well-Being</p>
                                <p class='par'>Designing for well-being is complex. To build healthy communities amid this complexity, it is essential to begin with a human-centered understanding of the challenges and opportunities people face today, while also imagining how life might change for tomorrow. This session will explore the important role of community and social factors in determining health and care options, and examples of how social determinants of health, design and insights can help play a role in realizing the larger vision.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought11.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Jim Mondi</p>
                                <p class="sub">Vice President, Digital Technology Consulting, Cognizant</p>
                                <p class='small green'>The Future of Interoperability: Requirements for a New HIT Era</p>
                                <p class='par'>While HIPAA legislation established patients as the primary owners of their patient data, they are the least likely stakeholders to have access to it. Over the past several decades many meaningful attempts have been made to increase patient access. However, industry barriers have proven too great to overcome. New regulations are posed to address the barriers to ultimately increase interoperability and patient access. This session will discuss the future of interoperability and the steps healthcare organizations will need to take to ensure a smooth migration into a new HIT era.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' >
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/thought12.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Robert Brown</p>
                                <p class="sub">AVP, Center for the Future of Work, Cognizant</p>
                                <p class='small green'>What to do When Machines do Everything: 21 (More) Jobs of the Future</p>
                                <p class='par'>The rise of AI is fundamental to new breakthroughs in healthcare. But even when machines “do everything,” there will still be lots for doctors, nurses and healthcare payers and practitioners to do, it just won’t be the tasks we’re doing today. Based on new research from Cognizant’s Center for the Future of Work, this session will present actionable advice and guidance on how to navigate the Age of AI, Algorithms and Automation and explore the new healthcare jobs of the (not too distant) future that are emerging. You never know -- one day, YOU might be doing one of them!</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                    </div>
                </section>

                <section id='innovations' class='list'>
                    <div class="schedule_sessions timeit open innovation">
                        <div class='timeit' data-end="2019-05-22">
                            <div class='info'>
                                <p class="thin">Advancing a Platform Strategy Across Payers and Providers: From Revenue Cycle to Population Health, See New & Innovative Ways we are Enabling Differentiation in this Space</p>
                                <p class='small green'>TUESDAY, MAY 21</p>
                                <p class="small green">11:15 AM</p>
                                <p class='par'>See new innovative ways we are extending your investments, responding to changing market demands and offering solutions that allow you to adapt to this changing healthcare landscape. In this session, we will discuss the use and re-purpose of Payer claims data, Quality data, consumer data and financial data. As healthcare continues to move to more of a Consumer-centric and value-based world, these strategies will need to be contemplated.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-22">
                            <div class='info'>
                                <p class="thin">What to do When Machines do Everything: 21 (More) Jobs of the Future</p>
                                <p class='small green'>TUESDAY, MAY 21</p>
                                <p class="small green">1:15 PM</p>
                                <p class='par'>The rise of AI is fundamental to new breakthroughs in healthcare. But even when machines “do everything,” there will still be lots for doctors, nurses and healthcare payers and practitioners to do, it just won’t be the tasks we’re doing today. Based on new research from Cognizant’s Center for the Future of Work, this session will present actionable advice and guidance on how to navigate the Age of AI, Algorithms and Automation and explore the new healthcare jobs of the (not too distant) future that are emerging. You never know -- one day, YOU might be doing one of them!</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-22">
                            <div class='info'>
                                <p class="thin">Enterprise AI in Healthcare (Automate, Augment, Engage, Analysis)</p>
                                <p class='small green'>TUESDAY, MAY 21</p>
                                <p class="small green">2:30 PM</p>
                                <p class='par'>This session will address the need for and enterprise-wide approach to artificial intelligence, and four things AI will do in healthcare today: automation, augmentation, engagement, analysis…and the need for a different technology. Join us for an operational approach for each area.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-22">
                            <div class='info'>
                                <p class="thin">The Future of Care: Enhancing Community Health & Well-Being</p>
                                <p class='small green'>TUESDAY, MAY 21</p>
                                <p class="small green">4:00 PM</p>
                                <p class='par'>Designing for well-being is complex. To build healthy communities amid this complexity, it is essential to begin with a human-centered understanding of the challenges and opportunities people face today, while also imagining how life might change for tomorrow. This session will explore the important role of community and social factors in determining health and care options, and examples of how social determinants of health, design and insights can help play a role in realizing the larger vision.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='info'>
                                <p class="thin">TruProvider - A Comprehensive Provider Management Solution</p>
                                <p class='small green'>WEDNESDAY, MAY 22</p>
                                <p class="small green">10:00 AM</p>
                                <p class='par'>Cognizant solution for provider data management enabling payer organizations to efficiently operationalize, engage and comply with regulatory requirements.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='info'>
                                <p class="thin">The Future of Interoperability: Requirements for a New HIT Era</p>
                                <p class='small green'>WEDNESDAY, MAY 22</p>
                                <p class="small green">11:15 AM</p>
                                <p class='par'>​While HIPAA legislation established patients as the primary owners of their patient data, they are the least likely stakeholders to have access to it. Over the past several decades many meaningful attempts have been made to increase patient access. However, industry barriers have proven too great to overcome. New regulations are posed to address the barriers to ultimately increase interoperability and patient access. This session will discuss the future of interoperability and the steps healthcare organizations will need to take to ensure a smooth migration into a new HIT era.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                    </div>
                </section>
            </section>

<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>
<script>

$('.main_nav p').click(function(){
    var id = $(this).attr('data-id');
    $('.main_nav p').removeClass('active');
    $(this).addClass('active');
    $('.schedule_sessions').fadeOut(500);
    setTimeout(function(){
        $('#' + id).fadeIn(500);
        $('#' + id + ' > div').fadeIn(500);
    }, 500);
});

$('.main_nav p').click(function(){
    var id = $(this).html().toLowerCase();

    $('.main_nav p').removeClass('active');
    $(this).addClass('active');

    $('.days, .list').fadeOut(500);
    setTimeout(function(){
        $('.' + id + ', #' + id).fadeIn(500);
        $('.' + id).css('display', 'flex');
    }, 500);
});

$('#session_nav').click(function(){
    $('#sessions > div:first-child').fadeIn(500);
    $('.sessions > div:first-child').addClass('active_day')
});

$('#schedule_nav').click(function(){
    $('#schedules > div:first-child').fadeIn(500);
    $('.schedules > div:first-child').addClass('active_day')
});

$('.speakers .timeit, .innovation .timeit').click(function(){
    var toggle = $(this).children('.toggle_info');
    if ( toggle.hasClass('hide_info') ) {
        toggle.removeClass('hide_info');
        toggle.children('p').html('+');
        $(this).css('align-items', 'center');
        $(this).children('.info').children('.par').slideUp(300);
    }else {
        $(this).css('align-items', 'flex-start');
        toggle.addClass('hide_info');
        toggle.children('p').html('–');
        $('.par').slideUp(300);
        $(this).children('.info').children('.par').slideDown(300);
    }
});

function dateFromDay(year, day){
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}



</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
            <!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
                <video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

            </section> -->

            <canvas id="qr-canvas"style="display:none">
            </canvas>

            <!-- Choosing pictures -->
            <div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

            <div id='qrimg' style="display:none">

            </div>

            <div id='webcamimg' style="display:none">

            </div>

            <!-- Alerts -->
            <section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
            </section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
