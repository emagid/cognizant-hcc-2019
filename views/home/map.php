<main>
    <section class="home" style='overflow: auto;'>

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo_blue.png"></a>
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

        <body>
            

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <div class='main_nav'>
                            <p id='reset' data-id='solution' class='active'>SOLUTION CENTER</p>
                            <p data-id='hotel' class=''>HOTEL</p>
                        </div>
                    </div>
                </div>

                <section id='solution' class='active list'>
                    <div class='text_holder'>
                        <h1>Click on map to Interact</h1>
                        <ul></ul>
                        <p></p>
                    </div>
                    <div class='map_holder'>
                        <img src="<?=FRONT_ASSETS?>img/map_back.png">
                        <?php include __DIR__."/map_svg.php";?>
                    </div>
                </section>

                <section id='hotel' class='list'>
                    <img src="<?=FRONT_ASSETS?>img/hotel_map.png">
                </section>
            </section>

<script>

$('.main_nav p').click(function(){
    var id = $(this).attr('data-id');

    $('.main_nav p').removeClass('active');
    $(this).addClass('active');

    $('.days, .list').slideUp(500);
    setTimeout(function(){
        $('#' + id).slideDown(500);
    }, 500);
});

var svgs = '#solution > div svg polyline, #solution > div svg rect, #solution > div svg polygon'
$(svgs).click(function(){
    $(svgs).removeClass('show_svg');
    $(this).addClass('show_svg');
    var title = $(this).attr('data-title');
    var obj = txt[title];

    $('h1').html(title);
    $('ul li').remove();
    if(obj.bullets){
        $('ul li').remove();
        obj.bullets.forEach(function(bullet){
            $('ul').append(`<li>${bullet}</li>`)
        });
    }
    $('.text_holder p').html('');
    if(obj.p){ $('.text_holder p').html(obj.p) };
});

$('#reset').click(function(){
    $('h1').html('Click on map to Interact');
    $('.text_holder p').html('');
    $('ul li').remove();
});
</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
            <!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
                <video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

            </section> -->

            <canvas id="qr-canvas"style="display:none">
            </canvas>

            <!-- Choosing pictures -->
            <div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

            <div id='qrimg' style="display:none">

            </div>

            <div id='webcamimg' style="display:none">

            </div>

            <!-- Alerts -->
            <section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
            </section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>

<script type="text/javascript">
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('#video').removeClass('small_img');
            $('#share_alert').removeClass('small_check');
            $('#share_alert img').removeClass('small_check_img');
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);

            setTimeout(function(){
                $('#video').addClass('small_img');
                $('#share_alert').addClass('small_check');
                $('#share_alert img').addClass('small_check_img');
            }, 3500);

            
        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });



</script>
