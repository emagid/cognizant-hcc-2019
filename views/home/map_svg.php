<svg xmlns="http://www.w3.org/2000/svg" width="540" height="960" viewBox="0 0 540 960">
  <defs>
    <style>
      .a {
        fill: #fff;
        opacity: 0;
      }

      .b {
        fill: #47b649;
      }
    </style>
  </defs>
  <polygon data-title='Digital Operations' class="b" points="242.1 381 335.6 381 335 652.4 244.1 649.6 242.1 381"/>
  <polygon data-title='Digital Systems & Technology' class="b" points="242.1 247 307.4 247 318.7 263.3 318.7 372.6 244.5 370.8 242.1 247"/>
  <polygon data-title='Innovations Gallery' class="b" points="318.7 192.9 318.7 240.2 318.7 249.2 340.1 271.8 404.8 271.8 427.4 250.9 430.2 192.9 318.7 192.9"/>
  <polyline data-title='Life Sciences' class="b" points="484.8 303.3 397.5 303.3 397.5 401.8 484.8 401.8"/>
  <polygon data-title='Provider & Health Systems' class="b" points="484.8 414.8 394.7 414.8 394.7 533 484.8 535.3 495.5 535.3 491 414.8 484.8 414.8"/>
  <rect data-title='Digital Business' class="b" x="397" y="584.3" width="100.8" height="133.46"/>
  <polygon data-title='3M Health Information Systems' class="b" points="394.7 544.9 419.2 544.9 419.4 560.5 393.6 560.5 394.7 544.9"/>
  <rect data-title='Hyland Healthcare' class="b" x="419.2" y="544.9" width="25.3" height="15.64"/>
  <rect data-title='ACI Universal Payments' class="b" x="393.9" y="560.7" width="25.3" height="15.64"/>
  <!-- <rect data-title='Venture Technologies' class="b" x="419.2" y="560.7" width="25.3" height="15.64"/> -->
  <rect data-title='The Phia Group' class="b" x="278" y="658.6" width="24.7" height="15.23"/>
  <rect data-title='Validic' class="b" x="302.8" y="658.6" width="24.7" height="15.23"/>
  <rect data-title='FlexTech' class="b" x="302.8" y="673.8" width="24.7" height="15.62"/>
  <rect data-title='Micro Dyn Medical Systems' class="b" x="278" y="673.8" width="24.7" height="15.62"/>
  <rect data-title='EvolveSPM' class="b" x="278.2" y="708.1" width="24.5" height="15.34"/>
  <rect data-title='Healthwise' class="b" x="302.7" y="708.1" width="25" height="15.34"/>
  <rect data-title='WexHealth' class="b" x="302.7" y="723.4" width="25" height="15.34"/>
  <rect data-title='Teladoc Health' class="b" x="278.2" y="723.6" width="24.5" height="15.34"/>
  <rect data-title='VMware Healthcare Solutions' class="b" x="272.5" y="747.9" width="27.4" height="27.35"/>
  <rect data-title='Cisco' class="b" x="299.9" y="747.9" width="27.4" height="27.35"/>
  <rect data-title='Optum' class="b" x="272.5" y="775.3" width="27.4" height="27.35"/>
  <rect data-title='IBM' class="b" x="299.9" y="775.3" width="27.4" height="27.35"/>
  <rect data-title='Change Healthcare' class="b" x="346.3" y="747.5" width="25" height="48.96"/>
  <rect data-title='Zelis Healthcare' class="b" x="371.4" y="747.9" width="24.2" height="48.49"/>
  <rect data-title='RedCard' class="b" x="415.3" y="748.3" width="24.4" height="48.74"/>
  <rect data-title='Informatica' class="b" x="439.7" y="735.9" width="24.8" height="24.78"/>
  <rect data-title='Quadient' class="b" x="439.7" y="760.3" width="24.8" height="24.78"/>
  <rect data-title='Adobe' class="b" x="439.7" y="785.1" width="24.8" height="24.35"/>
</svg>

<script type="text/javascript">
    var txt = {
        'Digital Systems & Technology': {
            bullets: [
                'Optimization Tools',
                'Quality Engineering & Assurance',
                'Healthcare Technical Services',
                'Product Support',
                'Configuration Center of Excellence',
                'HealthCheck Optimization Services',
                'Cognizant Security'
            ]
        },
        'Provider & Health Systems': {
          bullets: [
            'TranZform',
            'TranZform EngageProvider',
            'TranZform EngageConsumer',
            'TranZform Digital Therapeutics',
            'Touchless Authorization Processing (TTAP)'
          ]
        },
        'Innovations Gallery': {
        },
        'Life Sciences': {
            bullets: [
                'Digital Clinical',
                'Digital Therapeutics',
                'Enterprise Fusion',
                'Value-Based Care'
            ]
        },
        'Digital Operations': {
            bullets: [
                'CARE Management',
                'ClaimsExchange',
                'ClaimSphere',
                'Digital Process Services',
                'Evolution of QNXT',
                'Facets',
                'Healthcare Process Automation (HPA)',
                'Learning Services',
                'NetworX',
                'QicLink',
                'TriZetto Elements',
                'TruProvider'
            ]
        },
        'Digital Business': {
            bullets: [
                'Cognizant Interactive',
                'AI & Analytics',
                'Cognizant SoftVision',
                'Connected Products',
                'Digital Strategy',
                'Healthcare Digital Office',
                'Healthcare Technology Office'
            ]
        },
        '3M Health Information Systems': {
          p: '3M Health Information Systems works with providers, payers and government agencies to anticipate and navigate a changing healthcare landscape. 3M provides healthcare data aggregation, analysis and strategic services that help clients move from volume to value-based health care, resulting in millions of dollars in savings, improved provider performance and higher quality care. 3M’s innovative software is designed to raise the bar for computer-assisted coding, clinical documentation improvement, performance monitoring, quality outcomes reporting and terminology management. For more information, visit www.3m.com/his or follow @3MHISNews on Twitter.'
        },
        'Hyland Healthcare': {
          p: 'By combining information management and enterprise medical imaging with business process and case management capabilities, Hyland Healthcare delivers a suite of unparalleled content and image management solutions to address the needs of healthcare organizations around the world. Every day, more than 2,000 healthcare organizations use Hyland Healthcare’s solutions to empower healthcare providers, inform critical decision making, and simplify information access and insight. For more information, please visit Hyland.com/Healthcare.'
        },
        'ACI Universal Payments': {
          p: 'Save 19% of staff time spent on payments with ACI Worldwide’s simple billing and payment system. ACI Worldwide is a billing and payment systems expert, processing $14 trillion per day for 5,100 clients around the globe. ACI empowers you to use one partner to power all of your bills, payments and remittance advice while raising member satisfaction by 25%. Visit www.aciworldwide.com/healthcare for research on members’ payment demands, or contact us at billpay@aciworldwide.com or 402-390-7600.'
        },
        'Venture Technologies': {
          p: ''
        },
        'The Phia Group': {
          p: 'The Phia Group’s mission is to reduce the cost of healthcare with innovative technologies, legal expertise, and focused, flexible customer service.  The Phia Group is an experienced provider of healthcare cost containment techniques, offering comprehensive recovery, plan document, and consulting services designed to control plan expenses and protect plan assets.'
        },
        'Validic': {
          p: "Validic guides healthcare organizations through the technical complexities associated with accessing and operationalizing patient-generated health data. Validic's scalable, secure solutions help you improve operational efficiency and patient outcomes by delivering personal health data from hundreds of home health devices seamlessly into your existing clinical workflows. To find out how healthcare is innovating to create more data-driven and integrative healthcare experiences, visit validic.com or follow Validic on Twitter at @validic."
        },
        'FlexTech': {
          p: 'For 30 years, FlexTech has been the leading implementation consulting company in the healthcare payer marketplace.  Our services revolve around the implementation lifecycle of healthcare payer systems, including system assessment and procurement, project management, configuration, and operations readiness.  Our consultants average over 20 years of healthcare experience, and have participated in over 250 implementation projects involving every major healthcare system.  FlexTech has been dedicated exclusively to solving payer problems, and has formed collaborative partnerships with the leading healthcare payer technology vendors.  We continually expand our staff knowledge base with the latest tools, training, and information, enabling us to provide our clients with the most experienced and knowledgeable consultants available.  Enjoying a preferred partnership with Cognizant since 1997, FlexTech has provided QNXT and Facets consulting services to 100+ Cognizant clients.'
        },
        'Teladoc Health': {
          p: "Teladoc Health is the global virtual care leader, offering the only comprehensive virtual care solution spanning telehealth, expert medical, and licensed platform services. Through our award-winning consumer experience brands -- Advance Medical, Best Doctors, BetterHelp, HealthiestYou, and Teladoc – we help millions of people around the world resolve their healthcare needs with confidence. Teladoc Health serves more than 10,000 clients including the world's leading insurers, employers, hospitals and health systems. Together, we are continually modernizing the healthcare experience and making high-quality healthcare a reality for more people and organizations around the world. Learn more at TeladocHealth.com."
        },
        'EvolveSPM': {
          p: 'EvolveSPM solves the complex issue of paying commissions to internal agents, brokers and agencies correctly and on time. By using EvolveSPM, health plans optimize their incentive compensation and marketing plans - while staying compliant with CMS government regulations. EvolveSPM includes powerful features like flexible contracts, retro-active-reconciliation, membership management, broker management, goals, team, tiers, payment schedules, etc. EvolveSPM aggregates data from finance, sales, enrollment processing, broker relations, licensing, training, CMS and other client dependent data sources – a single data source from which complicated compensation can be managed.'
        },
        'Healthwise': {
          p: 'Since 1975, our mission has been to help people make better health decisions. With Healthwise and Cognizant’s TriZetto CareAdvance Enterprise, we power health partnerships by connecting people to tailored, meaningful health education anytime, anywhere. And show that it makes a difference. www.healthwise.org'
        },
        'WexHealth': {
          p: 'We simplify the business of healthcare through WEX Health Cloud, a cloud-based healthcare financial management platform that drives efficiency for benefit administration technology, consumer engagement, and advanced billing and payments. Our partner organizations enable us to deliver our industry-leading and award-winning solution to 300,000 employers and more than 25 million consumers. Together we take the complexity out of defined contribution, HSAs, HRAs, FSAs, VEBAs, PRAs, premium billing, public and private health insurance exchanges, COBRA, wellness plans, and transit plans. Learn more at https://www.wexinc.com/solutions/healthcare-benefits-management/, and follow WEX Health at @WEXHealthInc.'
        },
        'Micro Dyn Medical Systems': {
          p: 'Micro-Dyn Medical Systems, Inc. specializes in desktop and developer software products for DRG, APC, ASC, IPPS, IRF, ESRD, SNF and Home Health grouping, editing, and reimbursement calculation to Medicare specifications. Visit us at www.microdynmed.com.'
        },
        'VMware Healthcare Solutions': {
          p: 'VMware Healthcare Solutions are driving next-generation, patient-centric care by transforming the cost, quality, and delivery of patient care from the data center to the point of care. Our software-defined healthcare IT platform modernizes and protects critical IT infrastructure at the heart of value-based patient care while mobilizing providers with always-on access to patient information from the right device, for the right task, at the right time. With VMware, healthcare organizations are personalizing care and reducing hospital re-admits. They are using cloud to free resources for innovation and connecting rural clinics and retail pharma to regional hospitals to strengthen the care continuum. Learn more at http://www.vmware.com/go/healthcare.'
        },
        'Cisco': {
          p: "Cisco is the worldwide technology leader that has been making the Internet work since 1984. Our people, products, and partners help society securely connect and seize tomorrow's digital opportunity today."
        },
        'Optum': {
          p: "Optum is a leading health services and innovation company dedicated to helping make the health system work better for everyone. With more than 145,000 people worldwide, Optum combines technology, data and expertise to improve the delivery, quality and efficiency of health care. Optum uniquely collaborates with all participants in health care, connecting them with a shared focus on creating a healthier world. Hospitals, doctors, pharmacies, employers, health plans, government agencies and life sciences companies rely on Optum services and solutions to solve their most complex challenges and meet the growing needs of the people and communities they serve."
        },
        'IBM': {
          p: "IBM is a global technology and innovation company headquartered in Armonk, NY.   One of the largest technology and consulting employers in the world, with more than 375,000 employees serving clients in 170 countries.  Just completing its 22nd year of patent leadership, IBM Research has defined the future of information technology with more than 3,000 researchers in 12 labs located across six continents. Today, IBM is much more than a “hardware, software, services” company.  IBM has transformed itself into a leading cognitive solutions and cloud platform company."
        },
        'Change Healthcare': {
          p: "Change Healthcare is a key catalyst of a value-based healthcare system– working alongside customers to accelerate the journey towards improved lives and healthier communities.  We partner with TriZetto/Cognizant to provide advanced payment processing technology designed to reduce costs and improve member experience and Payment Integrity solutions to help payers combat the risk for payment of improper claims.   Learn more at www.changehealthcare.com."
        },
        'Zelis Healthcare': {
          p: "Zelis Healthcare is a healthcare technology company and market-leading provider of integrated healthcare cost management and payments solutions including network analytics and design, network access and cost management, claims cost management and electronic payments to payers, healthcare providers and consumers in the medical, dental and workers’ compensation markets nationwide.  Over 85% of the time, we identify additional network cost, claims cost and payments cost savings other companies are unable to find. That’s because we’re over 800 people working as one with payers and providers to deliver industry-leading levels of service and performance."
        },
        'RedCard': {
          p: "RedCard provides data management and BPO services for the production of printed or electronic documents, including checks, EOBs, enrollment materials, electronic payments, ID Cards, and letters. RedCard utilizes its proprietary DOCS® system to manage all facets of these communications with innovative document designs in full color. DOCS® is the single largest document processing platform in healthcare, and it enables payers to enjoy substantial savings while creating an enhanced member/provider experience."
        },
        'Informatica': {
          p: "Digital transformation changes expectations: better service, faster delivery, with less cost. Healthcare organizations must transform to stay relevant and data holds the answers."
        },
        'Quadient': {
          p: "Quadient helps companies deliver meaningful interactions with current and future customers. A Neopost Digital Company, the Quadient portfolio of technology enables organizations to create better experiences for their customers through timely, optimized, contextual, highly individualized, and accurate communications for all channels. Our solutions bring together and activate the entire organization in the name of customer experience, through better collaboration and visibility into the customer journey. Quadient supports thousands of clients and partners worldwide in the financial services, insurance and service provider industries in their quest to achieve customer experience excellence via mobile, digital, social media and print technologies."
        },
        'Adobe': {
          p: "Adobe gives everyone—from emerging artists to global brands—everything they need to design and deliver exceptional digital experiences. We’re passionate about empowering people to create beautiful and powerful images, videos, and apps, and transforming how companies interact with their customers across every screen."
        }

    }
</script>
