<main>
    <section class="sessions" style='overflow: auto;'>

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo_blue.png"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

        <body>
            

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <div class='main_nav two_nav'>
                            <p id='thought_nav' data-id='events' class='active'>EVENTS</p>
                            <p id='innovation_nav' data-id='leisure' class=''>LEISURE TIME</p>
                        </div>
                        <div class='schedule_buttons'>
                            <div class='leisure two_btns days'>
                                <div class="day active_day timeit" data-id="day_one">
                                    <p>Planned Activities</p>
                                </div>
                                <div class="day timeit" data-id="day_two">
                                    <p>AREA RESTAURANTS & BARS</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section id='events' class='active list'>
                    <div class="schedule_sessions timeit open speakers">
                        <div class='timeit' data-end="2019-05-21">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/event1.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Voodoo in the Bayou Partner Reception</p>
                                <p class="sub">Solution Center (Grand Ballroom)</p>
                                <p class='small green'>MONDAY, MAY 20 | 7:00 - 9:30 PM</p>
                                <p class='par'>Join our partners in the Solution Center tonight. It’s always a party in New Orleans so we are throwing a seafood boil down in the bayou, just like the ragin’ Cajuns! Learn how to enjoy the local delicacy, crawfish, while discovering the real truth about Voodoo, which is as much a part of New Orleans as Mardi Gras, jazz and the beignet.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/event2.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Mardi Gras Madness!</p>
                                <p class="sub">Hotel lobby</p>
                                <p class='small green'>WEDNESDAY, MAY 22 | 5:30 PM – 12:00 AM</p>
                                <p class='par'>Tonight will be a once-in-a-lifetime experience! Meet in the hotel lobby and Porte Cochere to get ready for the Cognizant Mardi Gras parade. You won’t be watching, you will be in it along with the Grand Marshall, brass bands, revelers, etc. Wear your Mardi Gras colors, purple, green and gold and comfortable walking shoes. We will provide you with beads to wear. Step off is promptly at 5:30 pm so please be on time.
                                <br>
                                <br>
                                From there, its anyone’s guess, but you will not want to miss it. As they say in New Orleans, LAISSEZ LE BON TEMPS ROULER…LET THE GOOD TIMES ROLL!</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                    </div>
                </section>

                <section id='leisure' class='list leisurepage'>
                    <div class="schedule_sessions timeit open speakers" id="day_one">
                        <div class='timeit' data-end="2019-05-21">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/leisure1.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Golf at Stonebridge Golf Club</p>
                                <p class="sub">MONDAY, MAY 20</p>
                                <p class='small green'>8:00 AM - Departure from the hotel lobby </p>
                                <p class='small green'>9:00 AM - Shotgun</p>
                                <p class='par'>Stonebridge Golf Club of New Orleans features a 18-hole Championship layout with picturesque views of the city. The course winds through several canals and bayous and guarantees you won’t have to look far to find one of many species of reptiles and waterfowl that make their home in this region. Sign up to golf when you register.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/leisure2.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Mysteries of Louisiana Swampland</p>
                                <p class="sub">WEDNESDAY, MAY 22</p>
                                <p class='small green'>1:30 PM – 4:45 PM </p>
                                <p class='small green'>Departs from the hotel lobby (Porte Cochere)</p>
                                <p class='par'>Join us for an incredible journey into the mysterious swamps and bayous of southern Louisiana. On the swamp boat tour, you will be introduced to the interesting history of our Louisiana wetlands and the rich Cajun heritage of the people who inhabit this region.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/leisure3.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Forever New Orleans</p>
                                <p class="sub">WEDNESDAY, MAY 22</p>
                                <p class='small green'>1:30 PM – 3:30 PM </p>
                                <p class='small green'>Departs from the hotel lobby (Porte Cochere)</p>
                                <p class='par'>The tour begins in the area where the French founded New Orleans almost 300 years ago: the historic and charming French Quarter.  At Jackson Square in the heart of French Quarter, site of St. Louis Cathedral, you will see famous landmarks and learn the fascinating history. Visit the historic St. Louis No. 3 Cemetery, and more.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                        <div class='timeit' data-end="2019-05-23">
                            <div class='img' style="background-image: url('<?=FRONT_ASSETS?>img/leisure4.jpg')"></div>
                            <div class='info'>
                                <p class="thin">Cajun and Creole Cuisine</p>
                                <p class="sub">WEDNESDAY, MAY 22</p>
                                <p class='small green'>1:30 PM – 4:45 PM</p>
                                <p class='small green'>Departs from the hotel lobby (Porte Cochere)</p>
                                <p class='par'>One of the best New Orleans chefs will teach you the techniques and secrets that get to the very soul of Cajun and Creole cooking. (Rule #1 – first you make a roux!) You will learn the differences in the various styles of cooking in Louisiana and receive copies of recipes to take home with you to dazzle family and friends with your newfound culinary skills.</p>
                            </div>
                            <div class='toggle_info'><p class='show_info'>+</p></div>
                        </div>
                    </div>
                    <div class="schedule_sessions timeit innovation food" id="day_two">
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Palace Cafe</p>
                                <p class='small green'>CREOLE, 0.1 MILES</p>
                                <p class="show_par">Their "Flavor of New Orleans" cuisine is served everyday at an upbeat and lively grand café owned and operated by Dickie Brennan, of the famed New Orleans restaurant family.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">K-Paul's Louisiana Kitchen</p>
                                <p class='small green'>CREOLE, 0.3 MILES</p>
                                <p class="show_par">K-Paul's menus change daily based entirely on the availability of fresh ingredients.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Red Fish Grill</p>
                                <p class='small green'>SEAFOOD, 0.2 MILES</p>
                                <p class="show_par">Their casual New Orleans seafood dominates a menu peppered with Big Easy favorites like Hickory Grilled Redfish, BBQ Oysters, Alligator Sausage & Seafood Gumbo, Double Chocolate Bread Pudding, and a wide variety of Gulf fish available every night.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Arnaud's</p>
                                <p class='small green'>CREOLE, 0.3 MILES</p>
                                <p class="show_par">Located steps off of Bourbon Street in the heart of the French Quarter, Arnaud’s offers classic Creole Cuisine and exemplary service in beautifully restored turn of the century dining rooms.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Acme Oyster House</p>
                                <p class='small green'>SEAFOOD, 0.3 MILES</p>
                                <p class="show_par">Acme Oyster House opened their doors in 1910 and they have been proudly serving up the best ice, cold oysters in town.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Galatoire's</p>
                                <p class='small green'>FRENCH, 0.4 MILES</p>
                                <p class="show_par">Galatoire’s has a tradition of serving authentic French Creole cuisine at a level that raises consistency to an art form.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Antoine's Restaurant</p>
                                <p class='small green'>FRENCH, 0.4 MILES</p>
                                <p class="show_par">For over 160 years, Antoine's Restaurant's excellent French-Creole cuisine, service, and atmosphere have combined to create an unmatched dining experience for both locals and visitors to New Orleans.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">NOLA</p>
                                <p class='small green'>AMERICAN, 0.5 MILES</p>
                                <p class="show_par">A Contemporary gem tucked into a historic yellow stucco townhouse, which features casual food--an exciting pastiche of Southern culinary delights.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Mother's Restaurant</p>
                                <p class='small green'>CREOLE, 0.5 MILES</p>
                                <p class="show_par">Mother’s Restaurant opened its doors in 1938 serving dishes such as Po’ Boys, Seafood, and Louisiana favorites.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Cafe du Monde</p>
                                <p class='small green'>COFFEE HOUSE, 0.6 MILES</p>
                                <p class="show_par">The Original Cafe Du Monde is a traditional coffee shop. The Cafe is open 24 hours a day, seven days a week. It closes only on Christmas Day and on the day an occasional Hurricane passes too close to New Orleans.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Muriel's Jackson Square</p>
                                <p class='small green'>CREOLE, 0.8 MILES</p>
                                <p class="show_par">Embrace the historical ambience, feel the energy, and taste the love in every bite as the flavors dance on your palate while you discover a local treasure.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Cochon</p>
                                <p class='small green'>CAJUN, 0.9 MILES</p>
                                <p class="show_par">At Cochon, Chef Link has reconnected with his culinary roots, serving the traditional Cajun Southern dishes he grew up with.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Melange d'Orleans</p>
                                <p class='small green'>FRENCH, 9.4 MILES</p>
                                <p class="show_par">Mélange restaurant is a mixture of the most renowned dishes from New Orleans' most famous restaurants accented by some of the chef's own culinary creations amidst the backdrop of jazz. The restaurant is a celebration of New Orleans and its reputation.</p>
                            </div>
                        </div>
                        <div class='timeit'>
                            <div class='info'>
                                <p class="thin">Commander's Palace</p>
                                <p class='small green'>CREOLE, 2.7 MILES</p>
                                <p class="show_par">Since 1880, Commander's Palace has been a New Orleans landmark known for award winning Creole-American cuisine.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </section>

<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>
<script>

$('.main_nav p').click(function(){
    var id = $(this).attr('data-id');
    $('.main_nav p').removeClass('active');
    $(this).addClass('active');
    $('.days').fadeOut(500);
    setTimeout(function(){
        $('#' + id + ', .days.' + id).fadeIn(500);
        $($('#' + id + ' .schedule_sessions')[0]).fadeIn(500)
        $('.days.' + id).css('display', 'flex');
    }, 500);
});

$('.day').click(function(){
    var id = $(this).attr('data-id');
    $('.schedule_sessions').fadeOut(500);
    $('.day').removeClass('active_day');
    $(this).addClass('active_day');
    setTimeout(function(){
        $('#' + id).fadeIn(500);
    }, 500);
});

$('.main_nav p').click(function(){
    var id = $(this).attr('data-id').toLowerCase();
    $('.schedule_sessions').fadeOut(500);
    $('.main_nav p').removeClass('active');
    $(this).addClass('active');
    $('.day').removeClass('active_day');

    $('.days, .list').fadeOut(500);
    setTimeout(function(){
        $('.' + id + ', #' + id).fadeIn(500);
        $('.' + id).css('display', 'flex');
        $($('#' + id + ' .schedule_sessions')[0]).fadeIn(500);
        $($('.' + id + ' .day')[0]).addClass('active_day');
    }, 500);
});

$('#session_nav').click(function(){
    $('#sessions > div:first-child').fadeIn(500);
    $('.sessions > div:first-child').addClass('active_day')
});

$('#schedule_nav').click(function(){
    $('#schedules > div:first-child').fadeIn(500);
    $('.schedules > div:first-child').addClass('active_day')
});

$('.speakers .timeit').click(function(){
    var toggle = $(this).children('.toggle_info');
    if ( toggle.hasClass('hide_info') ) {
        toggle.removeClass('hide_info');
        toggle.children('p').html('+');
        $(this).css('align-items', 'center');
        $(this).children('.info').children('.par').slideUp(300);
    }else {
        $(this).css('align-items', 'flex-start');
        toggle.addClass('hide_info');
        toggle.children('p').html('–');
        $('.par').slideUp(300);
        $(this).children('.info').children('.par').slideDown(300);
    }
});

function dateFromDay(year, day){
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}



</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
            <!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
                <video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

            </section> -->

            <canvas id="qr-canvas"style="display:none">
            </canvas>

            <!-- Choosing pictures -->
            <div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

            <div id='qrimg' style="display:none">

            </div>

            <div id='webcamimg' style="display:none">

            </div>

            <!-- Alerts -->
            <section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
            </section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
