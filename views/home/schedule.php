<main>
	<section class="home" style='overflow: auto;'>

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo_blue.png"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <div class='main_nav'>
                            <p id='schedule_nav' class='active'>SCHEDULE</p>
                            <p id='session_nav' class=''>SESSIONS</p>
                        </div>
                        <div class='schedule_buttons'>
                            <div class='schedule show four_btns days'>
                                <div class="day active_day timeit" data-id="day_one" data-end="2019-05-21">
                                    <p>Monday</p>
                                </div>
                                <div class="day timeit" data-id="day_two" data-end="2019-05-22">
                                    <p>Tuesday</p>
                                </div>
                                <div class="day timeit" data-id="day_three" data-end="2019-05-23">
                                    <p>Wednesday</p>
                                </div>
                                <div class="day timeit" data-id="day_four" data-end="2019-05-24">
                                    <p>Thursday</p>
                                </div>
                            </div>
                            <div class='sessions two_btns days'>
                                <div class="day timeit" data-id="day139" data-end="2019-05-20">
                                    <p>Sunday</p>
                                </div>
                                <div class="day timeit" data-id="day140" data-end="2019-05-21">
                                    <p>Monday</p>
                                </div>
                                <div class="day timeit" data-id="day141" data-end="2019-05-22">
                                    <p>Tuesday</p>
                                </div>
                                <div class="day timeit" data-id="day142" data-end="2019-05-23">
                                    <p>Wednesday</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section id='schedule' class='active list'>
                    <div class="schedule_sessions timeit open" id="day_one" data-end="2019-05-21">
                        <div class='timeit' >
                            <p class="small green">8:00 AM</p>
                            <p class="thin">Golf Tournament - Stonebridge Golf Club of New Orleans</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">1:00 PM – 3:00 PM</p>
                            <p class="thin">Registration, Solution Center & Partner Exhibits Open</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">5:00 PM – 7:30 PM</p>
                            <p class="thin">Welcome Address & Keynote: Walter Isaacson/p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">7:30 PM – 9:30 PM</p>
                            <p class="thin">Opening Reception & Partner Exhibits in Solution Center</p>
                        </div>
                    </div>
                
                    <div class="schedule_sessions timeit" id="day_two" data-end="2019-05-22">
                        <div class='timeit' >
                            <p class="small green">7:00 AM – 7:45 AM</p>
                            <p class="thin">Breakfast in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">7:45 AM – 9:15 AM</p>
                            <p class="thin">Cognizant General Session</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">9:15 AM – 9:30 AM</p>
                            <p class="thin">Break in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">9:30 AM – 11:00 AM</p>
                            <p class="thin">Solution General Sessions: Facets, QNXT, QicLink, Care, Government and Life Sciences</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">11:00 AM – 11:15 AM</p>
                            <p class="thin">Break in Solutions Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">11:15 AM – 12:15 PM</p>
                            <p class="thin">Solutions, Services & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">12:15 PM – 1:15 PM</p>
                            <p class="thin">Lunch in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">1:15 PM – 2:15 PM</p>
                            <p class="thin">Solutions, Services & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">2:15 PM – 2:30 PM</p>
                            <p class="thin">Break in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">2:30 PM – 3:30 PM</p>
                            <p class="thin">Solutions, Services & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">3:30 PM – 4:00 PM</p>
                            <p class="thin">Break in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">4:00 PM – 5:00 PM</p>
                            <p class="thin">Solutions, Services & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">5:00 PM</p>
                            <p class="thin">Open Evening</p>
                        </div>
                    </div>
                    
    <!--                TUESDAY-->
                    <div class="schedule_sessions timeit" id="day_three" data-end="2019-05-23">
                        
                        <div class='timeit'>
                            <p class="small green">6:30 AM – 7:15 AM</p>
                            <p class="thin">Breakfast in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">7:15 AM – 8:30 AM</p>
                            <p class="thin">Keynote: Deborah Lee James</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">8:45 AM – 9:45 AM</p>
                            <p class="thin">Solutions, Services & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">9:45 AM – 10:00 AM</p>
                            <p class="thin">Break in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">10:00 AM – 11:00 AM</p>
                            <p class="thin">Client/Partner & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">11:15 AM – 12:15 PM</p>
                            <p class="thin">Client/Partner & Thought Leadership Breakouts</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">12:15 PM – 1:15 PM</p>
                            <p class="thin">Lunch in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">1:30 PM – 5:00 PM</p>
                            <p class="thin">Leisure Activities at Locations Outside Hotel</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">5:30 PM – 12:00 AM</p>
                            <p class="thin">Theme Event</p>
                        </div>

                    </div>
                    
    <!--                WEDNESDAY-->
                    <div class="schedule_sessions timeit" id="day_four" data-end="2019-05-24">
                        <div class='timeit' >
                            <p class="small green">8:00 AM – 9:00 AM</p>
                            <p class="thin">Breakfast in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">9:15 AM – 10:15 AM</p>
                            <p class="thin">Industry Keynote: Ian Morrison</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">10:15 AM – 10:45 AM</p>
                            <p class="thin">Break in Solution Center & Partner Exhibits</p>
                        </div>
                        <div class='timeit'>
                            <p class="small green">10:45 AM – 12:30 PM</p>
                            <p class="thin">User Group Meetings: QNXT, QicLink, TCG</p>
                        </div>
                        <div class='timeit' >
                            <p class="small green">12:30 PM</p>
                            <p class="thin">Grab & Go Lunch</p>
                        </div>
                    </div>
                </section>

                <section id='sessions' class='list'>
                    <? foreach($model->sessions as $stamp => $date) {?>
                        <div class="schedule_sessions timeit" id="day<?=$stamp?>" data-end="<?= $date['thedateNext'] ?>">
                            <? foreach ($date['sessions'] as $session){ ?>
                                <div class='timeit' data-end="<?= $date['thedate'] . ' ' . substr($session->start_time, 0, -3) ?>">
                                    <p class="small green"><?=$session->start_time?></p>
                                    <p class="thin"><?=$session->title?></p>
                                    <p class='room blue'><?=$session->room?></p>
                                </div>
                            <? } ?>
                        </div>
                    <? } ?>
                </section>
            </section>

<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>
<script>

highlightSub('schedule');
showContent('schedule');

function highlightSub(id){
    var arr = [];

    var subs = $('.' + id + ' .day');
    subs.each(function(){
       if (!$(this).hasClass('hidden')) {
        arr.push($(this));
       }
    });

    arr[0].addClass('active_day');
}

function showContent(id){
    console.log(id + ' content')
    var arr = [];
    var lists = $('#' + id + ' .schedule_sessions');
    $($('.list')[0]).fadeIn(500);

    lists.each(function(){
       if (!$(this).hasClass('hidden')) {
        arr.push($(this));
       }
    });

    arr[0].fadeIn(500);
}


$('.list:first-child').fadeIn();

$('.day').click(function(){
    var id = $(this).attr('data-id');
    $('.day').removeClass('active_day');
    $(this).addClass('active_day');
    $('.schedule_sessions').fadeOut(500);
    setTimeout(function(){
        $('#' + id).fadeIn(500);
    }, 500);
});

$('.main_nav p').click(function(){
    var id = $(this).html().toLowerCase();
    $('.day').removeClass('active_day');
    $('.main_nav p').removeClass('active');
    $(this).addClass('active');

    $('.days, .list').fadeOut(500);
    setTimeout(function(){
        $('.' + id).fadeIn(500);
        $('.' + id).css('display', 'flex');
    }, 500);
});

function reset(id){
    $('.list').fadeOut(500);
    $('#' + id).fadeIn(500);
    showFirstNav(id);
    showFirstContent(id);
}

function showFirstNav(id) {
    var arr = [];

    var subs = $('.' + id + ' .day');
    subs.each(function(){
       if (!$(this).hasClass('hidden')) {
        arr.push($(this));
       }
    });

    arr[0].addClass('active_day');
}

function showFirstContent(id) {
    var arr = [];
    var lists = $('#' + id + ' .schedule_sessions');

    lists.each(function(){
       if (!$(this).hasClass('hidden')) {
        arr.push($(this));
       }
    });

    arr[0].fadeIn(500);
}

$('#schedule_nav').click(function(){
    reset('schedule');
});

$('#session_nav').click(function(){
    reset('sessions');
});

function dateFromDay(year, day){
  var date = new Date(year, 0); // initialize a date in `year-01-01`
  return new Date(date.setDate(day)); // add the number of days
}



</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
