<?php

namespace Model;

class Provider_Roles extends \Emagid\Core\Model {
  
    static $tablename = "provider_roles";
    public static $fields = ['provider_id','role_id'];

}
